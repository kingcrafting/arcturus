package com.eu.habbo.habbohotel.achievements;

public enum TalentTrackType
{
    /**
     * Talen track for citizenship.
     */
    CITIZENSHIP,

    /**
     * Talen track for helpers and guardians.
     */
    HELPER
}